#include <cassert>
#include <iostream>

__global__ void spmv(int n, int *csr_index, int *csr_cols, float *csr_weights,
		float *v, float *out) {
	auto thisThread = blockIdx.x * blockDim.x + threadIdx.x;
	auto numThreads = gridDim.x + blockDim.x;

	for (int i = thisThread; i < n; i += numThreads) {
		float c = 0;
		for(int l = csr_index[i]; l < csr_index[i + 1]; ++l)
			c += csr_weights[l] * v[csr_cols[l]];
		out[i] = c;
	}
}

int main(void) {
	int n = 2;
	int *csr_index;
	int *csr_cols;
	float *csr_weights;
	float *v;
	float *out;

	cudaMallocManaged(&csr_index, (n + 1) * sizeof(int));
	cudaMallocManaged(&csr_cols, 3 * sizeof(int));
	cudaMallocManaged(&csr_weights, 3 * sizeof(float));
	cudaMallocManaged(&v, n * sizeof(float));
	cudaMallocManaged(&out, n * sizeof(float));

	// Matrix, 1st row: (1, 1).
	csr_index[0] = 0;
	csr_cols[0] = 0; csr_weights[0] = 1;
	csr_cols[1] = 1; csr_weights[1] = 1;

	// Matrix, 2nd row: (0, 1).
	csr_index[1] = 2;
	csr_cols[2] = 1; csr_weights[2] = 1;

	// Matrix, past-the-end.
	csr_index[2] = 3;

	// Input vector.
	v[0] = 42;
	v[1] = 21;

	// Run kernel: 1 thread block, 2 threads/block.
	spmv<<<1, 2>>>(n, csr_index, csr_cols, csr_weights, v, out);

	// Wait for the kernel to finish.
	cudaDeviceSynchronize();

	std::cout << "result: (" << out[0] << ", " << out[1] << ")" << std::endl;

	cudaFree(csr_index);
	cudaFree(csr_cols);
	cudaFree(csr_weights);
	cudaFree(v);
	cudaFree(out);

	return 0;
}
