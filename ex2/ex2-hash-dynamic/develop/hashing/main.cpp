#include <algorithm>
#include <cassert>
#include <cstring>
#include <chrono>
#include <iostream>
#include <optional>
#include <random>
#include <unordered_map>
#include <vector>

int num_insertions = 1 << 26;
constexpr int wordsize = 31;

// This hash table uses linear probing.
struct static_table {
	static constexpr const char *name = "static";

	struct cell {
		int key;
		int value;
		bool valid = false;
	};

	static_table(float max_fill, int num_tables)
	: cells{new cell[m]{}} {
		(void)max_fill; // Ignore the fill factor.
		(void)num_tables;
	}

	// Note: to simply the implementation, destructors and copy/move constructors are missing.

	// This function turns a hash into an index in [0, m).
	// This computes h % m but since m is a power of 2,
	// we can compute the modulo by using a bitwise AND to cut off the leading binary digits.
	// As hash, we just use the integer key directly.
	int hash_to_index(int h) {
		return h & (m - 1);
	}

	void put(int k, int v) {
		int i = 0;

		while(true) {
			assert(i < m);

			auto idx = hash_to_index(k + i);
			auto &c = cells[idx];

			if(!c.valid) {
				c.key = k;
				c.value = v;
				c.valid = true;
				return;
			}

			if(c.key == k) {
				c.value = v;
				return;
			}

			++i;
		}
	}

	std::optional<int> get(int k) {
		int i = 0;

		while(true) {
			assert(i < m);

			auto idx = hash_to_index(k + i);
			auto &c = cells[idx];

			if(!c.valid)
				return std::nullopt;

			if(c.key == k)
				return c.value;

			++i;
		}
	}

	int m = 2 * num_insertions;
	cell *cells = nullptr;
};

// For comparsion, an implementation that uses std::unordered_map.
// This is apples-to-oranges since std::unordered_map does not respect our fill factor.
struct stl_table {
	static constexpr const char *name = "stl";

	stl_table(float max_fill, int num_tables) {
		(void)max_fill; // Ignore the fill factor.
		(void)num_tables;
	}

	void put(int k, int v) {
		map[k] = v;
	}

	std::optional<int> get(int k) {
		auto it = map.find(k);
		if(it == map.end())
			return std::nullopt;
		return it->second;
	}

	std::unordered_map<int, int> map;
};

// This hash table uses linear probing with dynamic growth.
struct dynamic_table {
	static constexpr const char *name = "dynamic";

	struct cell {
		int key;
		int value;
		bool valid = false;
	};

	dynamic_table(float max_fill, int num_tables) : cells{new cell[m]{}}, max_fill(max_fill) {
		(void)num_tables;
	}

	// Note: to simply the implementation, destructors and copy/move constructors are missing.

	// This function turns a hash into an index in [0, m).
	// This computes h % m but since m is a power of 2,
	// we can compute the modulo by using a bitwise AND to cut off the leading binary digits.
	// As hash, we just use the integer key directly.
	int hash_to_index(int h) {
		return h & (m - 1);
	}

	// new function for dynamic table growth
	void check_and_resize() {
		if((float)(num_elements+1) < max_fill * (float)m)  {
			// already enough capacity
			return;
		} 

		int old_m = m;
		m <<= 1;
		cell *old_cells = cells; 
		cells = new cell[m]{};
		num_elements = 0;
		for(int i = 0; i < old_m; ++i) {
			cell &c = old_cells[i];
			if(c.valid) {
				put(c.key, c.value);
			}
		}

		return;
	}

	void put(int k, int v) {
		int i = 0;

		// new
		check_and_resize();

		while(true) {
			assert(i < m);

			auto idx = hash_to_index(k + i);
			auto &c = cells[idx];

			if(!c.valid) {
				c.key = k;
				c.value = v;
				c.valid = true;

				num_elements++;
				return;
			}

			if(c.key == k) {
				c.value = v;
				return;
			}

			++i;
		}
	}

	std::optional<int> get(int k) {
		int i = 0;

		while(true) {
			assert(i < m);

			auto idx = hash_to_index(k + i);
			auto &c = cells[idx];

			if(!c.valid)
				return std::nullopt;

			if(c.key == k)
				return c.value;

			++i;
		}
	}

	int m = 8;
	int num_elements = 0;
	cell *cells = nullptr;
	float max_fill;
};

// This hash table uses linear probing with dynamic growth.
struct dynamic_scaling_table {
	static constexpr const char *name = "dynamic scaling";

	struct cell {
		int key;
		int value;
		bool valid = false;
	};

	dynamic_scaling_table(float max_fill, int num_tables) :
		cells{new cell[m]{}}, max_fill(max_fill) {
			(void)num_tables;
		}

	// Note: to simply the implementation, destructors and copy/move constructors are missing.

	// This function turns a hash into an index in [0, m).
	// use scaling: h * m / 2^31 
	int hash_to_index(int k, int i) {
		return ( ( (k * 1ll * m) / (1ll << 31) ) + i) & (m-1);
	}

	// new function for dynamic table growth
	void check_and_resize() {
		if((float)(num_elements+1) < max_fill * (float)m)  {
			// already enough capacity
			return;
		} 

		int old_m = m;
		m <<= 1;
		cell *old_cells = cells; 
		cells = new cell[m]{};
		num_elements = 0;
		for(int i = 0; i < old_m; ++i) {
			cell &c = old_cells[i];
			if(c.valid) {
				put(c.key, c.value);
			}
		}

		return;
	}

	void put(int k, int v) {
		int i = 0;

		// new
		check_and_resize();

		while(true) {
			assert(i < m);

			auto idx = hash_to_index(k, i);
			auto &c = cells[idx];

			if(!c.valid) {
				c.key = k;
				c.value = v;
				c.valid = true;

				num_elements++;
				return;
			}

			if(c.key == k) {
				c.value = v;
				return;
			}

			++i;
		}
	}

	std::optional<int> get(int k) {
		int i = 0;

		while(true) {
			assert(i < m);

			auto idx = hash_to_index(k, i);
			auto &c = cells[idx];

			if(!c.valid)
				return std::nullopt;

			if(c.key == k)
				return c.value;

			++i;
		}
	}

	int m = 8;
	int num_elements = 0;
	cell *cells = nullptr;
	float max_fill;
};

struct sub_table {
	static constexpr const char *name = "subtables_hashing";

	struct cell {
		int key;
		int value;
		bool valid = false;
	};

	sub_table(float max_fill, int num_subtables) :
		max_fill(max_fill), num_subtables(num_subtables),
		cells{new cell*[num_subtables]},
		m{new int[num_subtables]},
		num_elements{new int[num_subtables]} {
			for(int i = 0; i < num_subtables; i++) {
				m[i] = 8;
				cells[i] = new cell[m[i]]{};
				num_elements[i] = 0;
			}

			int ntbl = num_subtables;
			l_num_subtables = 0;
			while(ntbl > 1)
			{
				ntbl>>=1;
				l_num_subtables++;
			}
		}

	// Note: to simply the implementation, destructors and copy/move constructors are missing.

	// This function turns a hash into an index in [0, m).
	// use scaling: h * m / 2^31 
	int hash_to_index(int k, int i) {
		int table_id = get_table_id(k);
		cut_table_id(k);
		return ( (k + i) & (m[table_id]-1));
	}

	int get_table_id(int k)
	{
		int table_id = (k >> (wordsize - l_num_subtables));
		assert(table_id < num_subtables);
		return table_id;
	}
	void cut_table_id(int& k)
	{
		// int mask = (1ll << (wordsize - l_num_subtables + 1)) - 1;
		// k &= mask;
	}

	// new function for dynamic table growth
	void check_and_resize(int table_id) {
		int& m_ = m[table_id];
		cell*& cells_ = cells[table_id];
		int& num_elements_ = num_elements[table_id];

		if((float)(num_elements_+1) < max_fill * (float)m_)  {
			// already enough capacity
			return;
		} 

		int old_m = m_;
		m_ <<= 1;
		cell *old_cells = cells_;
		cells_ = new cell[m_]{};
		num_elements_ = 0;
		for(int i = 0; i < old_m; ++i) {
			cell &c = old_cells[i];
			if(c.valid) {
				put(c.key, c.value);
			}
		}

		return;
	}

	void put(int k, int v) {
		int i = 0;

		// new
		int table_id = get_table_id(k);
		check_and_resize(table_id);
		int &m_ = m[table_id];
		int &num_elements_ = num_elements[table_id];
		cell*& cells_ = cells[table_id];

		while(true) {
			assert(i < m_);

			auto idx = hash_to_index(k, i);
			auto &c = cells_[idx];

			if(!c.valid) {
				c.key = k;
				c.value = v;
				c.valid = true;

				num_elements_++;
				return;
			}

			if(c.key == k) {
				c.value = v;
				return;
			}

			++i;
		}
	}

	std::optional<int> get(int k) {
		int table_id = get_table_id(k);
		int& m_ = m[table_id];
		cell*& cells_ = cells[table_id];


		int i = 0;
		while(true) {
			assert(i < m_);

			auto idx = hash_to_index(k, i);
			auto &c = cells_[idx];

			if(!c.valid)
				return std::nullopt;

			if(c.key == k)
				return c.value;

			++i;
		}
	}

	float max_fill;
	int num_subtables;
	cell **cells = nullptr;
	int *m;
	int *num_elements;
	int l_num_subtables;
};

// Helper function to perform a microbenchmark.
// You should not need to touch this.
template<typename Algo>
void microbenchmark(float max_fill, int num_tables) {
	Algo table{max_fill, num_tables};

	std::mt19937 prng{42};
	std::uniform_int_distribution<int> distrib;

	std::cerr << "Running microbenchmark..." << std::endl;

	auto start = std::chrono::high_resolution_clock::now();
	int nv = 1;
	for(int i = 0; i < num_insertions; ++i)
		table.put(distrib(prng), nv++);
	auto t = std::chrono::high_resolution_clock::now() - start;

	std::cout << "algo: " << Algo::name << std::endl;
	std::cout << "max_fill: " << max_fill << std::endl;
	std::cout << "num_tables: " << num_tables << std::endl;
	std::cout << "time: "
			<< std::chrono::duration_cast<std::chrono::milliseconds>(t).count()
			<< " # ms" << std::endl;
}

static const char *usage_text =
	"Usage: hashing [OPTIONS]\n"
	"Possible OPTIONS are:\n"
	"    --algo ALGORITHM\n"
	"        Select an algorithm {static,stl,dynamic,scaling,subtable}.\n"
	"    --max-fill FACTOR\n"
	"        Set the maximal fill factor before the table grows.\n"
	"    --num-tables TABLES\n"
	"        Set the number of subtables.\n";

int main(int argc, char **argv) {
	std::string_view algorithm;
	float max_fill = 0.5;
	int num_tables = 4;

	auto error = [] (const char *text) {
		std::cerr << usage_text << "Usage error: " << text << std::endl;
		exit(2);
	};

	// Argument for unary options.
	const char *arg;

	// Parse all options here.

	char **p = argv + 1;

	auto handle_unary_option = [&] (const char *name) -> bool {
		assert(*p);
		if(std::strcmp(*p, name))
			return false;
		++p;
		if(!(*p))
			error("expected argument for unary option");
		arg = *p;
		++p;
		return true;
	};

	while(*p && !std::strncmp(*p, "--", 2)) {
		if(handle_unary_option("--algo")) {
			algorithm = arg;
		}else if(handle_unary_option("--max-fill")) {
			max_fill = std::atof(arg);
		}else if(handle_unary_option("--num-tables")) {
			num_tables = std::atoi(arg);
		}else{
			error("unknown command line option");
		}
	}

	if(*p)
		error("unexpected arguments");

	// Verify that options are correct and run the algorithm.

	if(algorithm.empty())
		error("no algorithm specified");

	if(algorithm == "static") {
		microbenchmark<static_table>(max_fill, num_tables);
	}else if(algorithm == "stl") {
		microbenchmark<stl_table>(max_fill, num_tables);
	}else if(algorithm == "dynamic") {
		microbenchmark<dynamic_table>(max_fill, num_tables);
	}else if(algorithm == "scaling") {
		microbenchmark<dynamic_scaling_table>(max_fill, num_tables);
	}else if(algorithm == "subtable") {
		microbenchmark<sub_table>(max_fill, num_tables);
	}else{
		error("unknown algorithm");
	}
}
