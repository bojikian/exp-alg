#include <omp.h>
#include <atomic>
#include <chrono>
#include <cstring>
#include <climits>
#include <fstream>
#include <memory>
#include <unordered_map>
#include <random>

#include "csr.hpp"

struct dijkstra {
	void run(const csr_matrix &mat, unsigned int s) {
		d.resize(mat.n);

		std::fill(d.begin(), d.end(), FLT_MAX);
		d[s] = 0;
		pq.push(s);

		while(!pq.empty()) {
			auto u = pq.extract_top();

			// Iterate over the neighbors in CSR style.
			for(unsigned int i = mat.ind[u]; i < mat.ind[u + 1]; ++i) {
				auto v = mat.cols[i];
				auto weight = mat.weights[i];
				
				if(d[v] > d[u] + weight) {
					d[v] = d[u] + weight;
					pq.update(v);
				}
			}
		}
	}

private:
	class compare {
	public:
		bool operator() (unsigned int u, unsigned int v) const {
			return self->d[u] < self->d[v];
		}

		dijkstra *self;
	};

	std::vector<float> d;
	DAryAddressableIntHeap<unsigned int, 2, compare> pq{compare{this}};
};

struct bellman_ford {
	void run(const csr_matrix &tr, unsigned int s) {
		d.resize(tr.n);
		d_new.resize(tr.n);

		std::fill(d.begin(), d.end(), FLT_MAX);
		d[s] = 0;

		bool changes = false;
		bool another_iter;

		#pragma omp parallel
		{
			do {
				#pragma omp for reduction(||: changes)
				for(unsigned int v = 0; v < tr.n; ++v) {
					d_new[v] = d[v];

					for(unsigned int i = tr.ind[v]; i < tr.ind[v + 1]; ++i) {
						auto u = tr.cols[i]; // Kante: v -> u in tr, u -> v im Input.
						auto weight = tr.weights[i];

						if(d_new[v] > d[u] + weight) {
							d_new[v] = d[u] + weight;
							changes = true;
						}
					}
				}
				
				#pragma omp single
				{
					std::swap(d, d_new);
					another_iter = changes;
					changes = false;
				}
			} while(another_iter);
		}
	}

private:
	std::vector<float> d;
	std::vector<float> d_new;
};

struct dart_slot
{
	bool hit;
	unsigned int u;
	float d;
	dart_slot(bool h, unsigned int u, float d):hit(h), u(u), d(u){};
};
constexpr unsigned int SLOTS_PER_THREAD = 100;
constexpr unsigned int NUM_TRIALS = 15;

struct delta_stepping_const {
	delta_stepping_const(csr_matrix mat_)
	: mat{std::move(mat_)} { }

	void shortcut() {
		using vertex_pair = std::tuple<unsigned int, unsigned int>;
		using dist_triple = std::tuple<unsigned int, unsigned int, float>;

		struct pair_hash {
			size_t operator() (vertex_pair vp) const {
				return 13 * std::get<0>(vp) + std::get<1>(vp);
			}
		};

		struct shortcut {
			float d;
			bool newly_found;
		};

		std::unordered_map<vertex_pair, shortcut, pair_hash> dist;

		bool duplicates = false;
		for(unsigned int u = 0; u < mat.n; ++u) {
			for(unsigned int i = mat.ind[u]; i < mat.ind[u + 1]; ++i) {
				auto v = mat.cols[i];
				auto weight = mat.weights[i];

				auto it = dist.find({u, v});
				if(it == dist.end()) {
					dist.insert({{u, v}, {weight, false}});
				}else if(it->second.d > weight) {
					duplicates = true;
					it->second = {weight, false};
				}
			}
		}

		std::vector<dist_triple> q;
		for(unsigned int u = 0; u < mat.n; ++u)
			q.push_back({u, u, 0});

		std::vector<dist_triple> new_q;
		while(!q.empty()) {
			assert(new_q.empty());

			for(auto t : q) {
				auto u = std::get<0>(t);
				auto v = std::get<1>(t);
				auto d = std::get<2>(t);

				for(unsigned int i = mat.ind[v]; i < mat.ind[v + 1]; ++i) {
					auto x = mat.cols[i];
					auto weight = mat.weights[i];
					if(d + weight >= delta)
						continue;
					new_q.push_back({u, x, d + weight});
				}
			}
			q.clear();

			for(auto t : new_q) {
				auto u = std::get<0>(t);
				auto v = std::get<1>(t);
				auto d = std::get<2>(t);

				auto it = dist.find({u, v});
				if(it == dist.end()) {
					dist.insert({{u, v}, {d, true}});
				}else if(it->second.d > d) {
					it->second = {d, true};
				}
			}

			std::swap(q, new_q);
		}

		// Rebuild the CSR.
		assert(q.empty());

		for(auto kv : dist) {
			auto u = std::get<0>(kv.first);
			auto v = std::get<1>(kv.first);
			auto sc = kv.second;
			if(!sc.newly_found)
				continue;
			q.push_back({u, v, sc.d});
		}

		std::cerr << "found " << q.size() << " shortcuts" << std::endl;

		for(unsigned int u = 0; u < mat.n; ++u) {
			for(unsigned int i = mat.ind[u]; i < mat.ind[u + 1]; ++i) {
				auto v = mat.cols[i];
				auto weight = mat.weights[i];
				auto it = dist.find({u, v});
				if(it != dist.end()) {
					auto sc = it->second;
					if(sc.newly_found)
						continue;
				}
				q.push_back({u, v, weight});
			}
		}

		mat = coordinates_to_csr(std::move(q));
	}

	void prepare() {
		shortcut();
	}

	void run(unsigned int s) {
		using dist_pair =   std::pair<unsigned int, float>;

		auto nt = omp_get_max_threads();

		std::mt19937 prng{42};
		std::uniform_int_distribution<unsigned int> choose_slot_id{0, SLOTS_PER_THREAD - 1};

		// Vector of distances.
		std::vector<float> dist;
		dist.resize(mat.n);
		std::fill(dist.begin(), dist.end(), FLT_MAX);

		// Thrown darts.
		// There are nt-many entries per vertex of the graph,
		// such that all threads can throw darts without collisions.
		std::vector<float> darts;
		darts.resize(mat.n * nt);
		std::fill(darts.begin(), darts.end(), FLT_MAX);

		std::vector<dart_slot> dartslots;
		dartslots.assign( SLOTS_PER_THREAD * nt , dart_slot(false, 0, FLT_MAX));
		std::vector<std::vector<dist_pair>> waiting_darts(nt);
		std::vector<std::vector<dist_pair>> old_waiting_darts(nt);

		// Each thread gets its own PQ.
		auto compare = [&] (unsigned int u, unsigned int v) {
			return dist[u] < dist[v];
		};

		using pq_type = DAryAddressableIntHeap<unsigned int, 2, decltype(compare)>;
		std::vector<std::unique_ptr<pq_type>> pqs;
		for(int t = 0; t < nt; ++t)
			pqs.push_back(std::make_unique<pq_type>(compare));

		// A certain thread is responsible for the source.
		dist[s] = 0;
		pqs[s % nt]->push(s);

		#pragma omp parallel
		{
			auto ct = omp_get_thread_num();

			// Functions
			auto throw_dart = [&] (unsigned int u, float d) {
				unsigned int idx = u * nt + ct;
				if(darts[idx] > d)
					darts[idx] = d;
			};

			auto collect_darts = [&] (bool update_pq) {
				for(unsigned int u = ct; u < mat.n; u += nt) {
					for(unsigned int idx = u * nt; idx < (u + 1) * nt; ++idx) {
						if(dist[u] > darts[idx]) {
							dist[u] = darts[idx];
							if(update_pq)
								pqs[ct]->update(u);
						}
					}
				}
			};

			auto throw_dart_const = [&] (unsigned int u, float d) {
				unsigned int tries = NUM_TRIALS;
				bool success = false;
				while(!success && tries--)
				{
					unsigned int thrd = u % nt;
					unsigned int idx = thrd * SLOTS_PER_THREAD
						+ choose_slot_id(prng);
					bool in_use;
					bool& hit = dartslots[idx].hit;
					#pragma omp atomic capture
					{ in_use = hit; hit = true; }
					if(in_use)
					{
						continue;
					}
					dartslots[idx].u = u;
					dartslots[idx].d = d;
				}
				if(!success)
				{
					waiting_darts[u % nt].push_back(std::make_pair(u, d));
				}
			};

			//[todo] throw failed darts
			auto collect_darts_const = [&] (bool update_pq) {
				do
				{
					for(unsigned int idx = ct * SLOTS_PER_THREAD;
							idx < ct * (SLOTS_PER_THREAD+1); idx++)
					{
						if(!dartslots[idx].hit)
						{
							continue;
						}
						unsigned int u = dartslots[idx].u;
						unsigned int d = dartslots[idx].d;
						if(dist[u] > d)
						{
							dist[u] = d;
							if(update_pq)
							{
								pqs[ct]->update(u);
							}
						}


					}
					
					fill(dartslots.begin() + ct * SLOTS_PER_THREAD,
							dartslots.begin() + ct
							* (SLOTS_PER_THREAD + 1),
							dart_slot(false, 0, FLT_MAX));

					swap(waiting_darts[ct], old_waiting_darts[ct]);
					waiting_darts[ct].clear();
					for(auto p : waiting_darts[ct])
					{
						throw_dart_const(p.first, p.second);
					}
				} while(waiting_darts[ct].size());
			};
			std::vector<unsigned int> s;

			while(true) {
				#pragma omp barrier // To make sure all threads know dist[u] below.

				// First, find the next non-empty bucket.
				unsigned int cb = UINT_MAX;
				for(int t = 0; t < nt; ++t) {
					if(pqs[t]->empty())
						continue;
					unsigned int u = pqs[t]->top();
					unsigned int b = static_cast<unsigned int>(dist[u] / delta);
					if(cb > b)
						cb = b;
				}
				if(cb == UINT_MAX)
					break;

				#pragma omp barrier // Since we drain the PQs below.

				// Collect all vertices in the current bucket.
				s.clear();
				while(!pqs[ct]->empty()) {
					unsigned int u = pqs[ct]->top();
					if(!(dist[u] < (cb + 1) * delta))
						break;
					pqs[ct]->extract_top();
					s.push_back(u);
				}

				// First, handle intra-bucket edges.
				for(unsigned int u : s) {
					for(unsigned int i = mat.ind[u]; i < mat.ind[u + 1]; ++i) {
						auto v = mat.cols[i];
						auto weight = mat.weights[i];
						if(!(dist[u] + weight < (cb + 1) * delta))
							continue;
						throw_dart_const(v, dist[u] + weight);
					}
				}

				#pragma omp barrier
				collect_darts_const(false);

				// Now, handle inter-bucket edges.
				for(unsigned int u : s) {
					for(unsigned int i = mat.ind[u]; i < mat.ind[u + 1]; ++i) {
						auto v = mat.cols[i];
						auto weight = mat.weights[i];
						if(dist[u] + weight < (cb + 1) * delta)
							continue;
						throw_dart_const(v, dist[u] + weight);
					}
				}

				#pragma omp barrier
				collect_darts_const(true);
			}
		}
	}

private:
	csr_matrix mat;
	float delta = 0.5;
};



struct delta_stepping {
	delta_stepping(csr_matrix mat_)
	: mat{std::move(mat_)} { }

	void shortcut() {
		using vertex_pair = std::tuple<unsigned int, unsigned int>;
		using dist_triple = std::tuple<unsigned int, unsigned int, float>;

		struct pair_hash {
			size_t operator() (vertex_pair vp) const {
				return 13 * std::get<0>(vp) + std::get<1>(vp);
			}
		};

		struct shortcut {
			float d;
			bool newly_found;
		};

		std::unordered_map<vertex_pair, shortcut, pair_hash> dist;

		bool duplicates = false;
		for(unsigned int u = 0; u < mat.n; ++u) {
			for(unsigned int i = mat.ind[u]; i < mat.ind[u + 1]; ++i) {
				auto v = mat.cols[i];
				auto weight = mat.weights[i];

				auto it = dist.find({u, v});
				if(it == dist.end()) {
					dist.insert({{u, v}, {weight, false}});
				}else if(it->second.d > weight) {
					duplicates = true;
					it->second = {weight, false};
				}
			}
		}

		std::vector<dist_triple> q;
		for(unsigned int u = 0; u < mat.n; ++u)
			q.push_back({u, u, 0});

		std::vector<dist_triple> new_q;
		while(!q.empty()) {
			assert(new_q.empty());

			for(auto t : q) {
				auto u = std::get<0>(t);
				auto v = std::get<1>(t);
				auto d = std::get<2>(t);

				for(unsigned int i = mat.ind[v]; i < mat.ind[v + 1]; ++i) {
					auto x = mat.cols[i];
					auto weight = mat.weights[i];
					if(d + weight >= delta)
						continue;
					new_q.push_back({u, x, d + weight});
				}
			}
			q.clear();

			for(auto t : new_q) {
				auto u = std::get<0>(t);
				auto v = std::get<1>(t);
				auto d = std::get<2>(t);

				auto it = dist.find({u, v});
				if(it == dist.end()) {
					dist.insert({{u, v}, {d, true}});
				}else if(it->second.d > d) {
					it->second = {d, true};
				}
			}

			std::swap(q, new_q);
		}

		// Rebuild the CSR.
		assert(q.empty());

		for(auto kv : dist) {
			auto u = std::get<0>(kv.first);
			auto v = std::get<1>(kv.first);
			auto sc = kv.second;
			if(!sc.newly_found)
				continue;
			q.push_back({u, v, sc.d});
		}

		std::cerr << "found " << q.size() << " shortcuts" << std::endl;

		for(unsigned int u = 0; u < mat.n; ++u) {
			for(unsigned int i = mat.ind[u]; i < mat.ind[u + 1]; ++i) {
				auto v = mat.cols[i];
				auto weight = mat.weights[i];
				auto it = dist.find({u, v});
				if(it != dist.end()) {
					auto sc = it->second;
					if(sc.newly_found)
						continue;
				}
				q.push_back({u, v, weight});
			}
		}

		mat = coordinates_to_csr(std::move(q));
	}

	void prepare() {
		shortcut();
	}

	void run(unsigned int s) {
		auto nt = omp_get_max_threads();

		// Vector of distances.
		std::vector<float> dist;
		dist.resize(mat.n);
		std::fill(dist.begin(), dist.end(), FLT_MAX);

		// Thrown darts.
		// There are nt-many entries per vertex of the graph,
		// such that all threads can throw darts without collisions.
		std::vector<float> darts;
		darts.resize(mat.n * nt);
		std::fill(darts.begin(), darts.end(), FLT_MAX);

		// Each thread gets its own PQ.
		auto compare = [&] (unsigned int u, unsigned int v) {
			return dist[u] < dist[v];
		};

		using pq_type = DAryAddressableIntHeap<unsigned int, 2, decltype(compare)>;
		std::vector<std::unique_ptr<pq_type>> pqs;
		for(int t = 0; t < nt; ++t)
			pqs.push_back(std::make_unique<pq_type>(compare));

		// A certain thread is responsible for the source.
		dist[s] = 0;
		pqs[s % nt]->push(s);

		#pragma omp parallel
		{
			auto ct = omp_get_thread_num();

			// Functions
			auto throw_dart = [&] (unsigned int u, float d) {
				unsigned int idx = u * nt + ct;
				if(darts[idx] > d)
					darts[idx] = d;
			};

			auto collect_darts = [&] (bool update_pq) {
				for(unsigned int u = ct; u < mat.n; u += nt) {
					for(unsigned int idx = u * nt; idx < (u + 1) * nt; ++idx) {
						if(dist[u] > darts[idx]) {
							dist[u] = darts[idx];
							if(update_pq)
								pqs[ct]->update(u);
						}
					}
				}
			};

			std::vector<unsigned int> s;

			while(true) {
				#pragma omp barrier // To make sure all threads know dist[u] below.

				// First, find the next non-empty bucket.
				unsigned int cb = UINT_MAX;
				for(int t = 0; t < nt; ++t) {
					if(pqs[t]->empty())
						continue;
					unsigned int u = pqs[t]->top();
					unsigned int b = static_cast<unsigned int>(dist[u] / delta);
					if(cb > b)
						cb = b;
				}
				if(cb == UINT_MAX)
					break;

				#pragma omp barrier // Since we drain the PQs below.

				// Collect all vertices in the current bucket.
				s.clear();
				while(!pqs[ct]->empty()) {
					unsigned int u = pqs[ct]->top();
					if(!(dist[u] < (cb + 1) * delta))
						break;
					pqs[ct]->extract_top();
					s.push_back(u);
				}

				// First, handle intra-bucket edges.
				for(unsigned int u : s) {
					for(unsigned int i = mat.ind[u]; i < mat.ind[u + 1]; ++i) {
						auto v = mat.cols[i];
						auto weight = mat.weights[i];
						if(!(dist[u] + weight < (cb + 1) * delta))
							continue;
						throw_dart(v, dist[u] + weight);
					}
				}

				#pragma omp barrier
				collect_darts(false);

				// Now, handle inter-bucket edges.
				for(unsigned int u : s) {
					for(unsigned int i = mat.ind[u]; i < mat.ind[u + 1]; ++i) {
						auto v = mat.cols[i];
						auto weight = mat.weights[i];
						if(dist[u] + weight < (cb + 1) * delta)
							continue;
						throw_dart(v, dist[u] + weight);
					}
				}

				#pragma omp barrier
				collect_darts(true);
			}
		}
	}

private:
	csr_matrix mat;
	float delta = 0.5;
};

int main(int argc, char **argv) {
	if(argc != 3)
		throw std::runtime_error("Expected algorithm and instance as argument");

	std::mt19937 prng{42};
	std::uniform_real_distribution<float> weight_distrib{0.0f, 1.0f};

	// Load the graph.
	std::cout << "algorithm: " << argv[1] << std::endl;
	std::cout << "instance: " << argv[2] << std::endl;
	std::cout << "threads: " << omp_get_max_threads() << std::endl;

	std::ifstream ins(argv[2]);
	std::vector<std::tuple<unsigned int, unsigned int, float>> cv;

	auto io_start = std::chrono::high_resolution_clock::now();
	read_graph_unweighted(ins, [&] (unsigned int u, unsigned int v) {
		// Generate a random edge weight in [a, b).
		cv.push_back({u, v, weight_distrib(prng)});
	});

	auto mat = coordinates_to_csr(std::move(cv));
	auto t_io = std::chrono::high_resolution_clock::now() - io_start;

	std::cout << "time_io: "
			<< std::chrono::duration_cast<std::chrono::milliseconds>(t_io).count() << std::endl;
	std::cout << "n_nodes: " << mat.n << std::endl;
	std::cout << "n_edges: " << mat.nnz << std::endl;

	// Compute the transpose (for Bellman-Ford).
	auto tr = transpose(mat);

	std::uniform_int_distribution<unsigned int> s_distrib{0, mat.n - 1};

	if(!strcmp(argv[1], "dijkstra")) {
		dijkstra algo;

		auto algo_start = std::chrono::high_resolution_clock::now();
		algo.run(mat, s_distrib(prng));
		auto t_algo = std::chrono::high_resolution_clock::now() - algo_start;
		std::cout << "time_sssp: "
				<< std::chrono::duration_cast<std::chrono::milliseconds>(t_algo).count()
				<< std::endl;
	}else if(!strcmp(argv[1], "bf")) {
		bellman_ford algo;

		auto algo_start = std::chrono::high_resolution_clock::now();
		algo.run(tr, s_distrib(prng));
		auto t_algo = std::chrono::high_resolution_clock::now() - algo_start;
		std::cout << "time_sssp: "
				<< std::chrono::duration_cast<std::chrono::milliseconds>(t_algo).count()
				<< std::endl;
	}else if(!strcmp(argv[1], "delta")) {
		delta_stepping algo{std::move(mat)};
		algo.shortcut();

		auto algo_start = std::chrono::high_resolution_clock::now();
		algo.run(s_distrib(prng));
		auto t_algo = std::chrono::high_resolution_clock::now() - algo_start;
		std::cout << "time_sssp: "
				<< std::chrono::duration_cast<std::chrono::milliseconds>(t_algo).count()
				<< std::endl;
	}else if(!strcmp(argv[1], "delta-const")) {
		delta_stepping_const algo{std::move(mat)};
		algo.shortcut();

		auto algo_start = std::chrono::high_resolution_clock::now();
		algo.run(s_distrib(prng));
		auto t_algo = std::chrono::high_resolution_clock::now() - algo_start;
		std::cout << "time_sssp: "
				<< std::chrono::duration_cast<std::chrono::milliseconds>(t_algo).count()
				<< std::endl;
	}else{
		throw std::runtime_error("Unexpected algorithm");
	}
}
