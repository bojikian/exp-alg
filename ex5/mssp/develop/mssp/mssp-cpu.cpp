#include <omp.h>
#include <chrono>
#include <cstring>
#include <fstream>
#include <random>
#include <immintrin.h>
#include "csr.hpp"


inline bool eq_vec(__m256 lhs, __m256 rhs)
{
	__m256 zero = _mm256_set1_ps(0.0);
	auto c = _mm256_cmp_ps(lhs, rhs, _CMP_NEQ_OQ);
	return _mm256_testc_ps(zero, c);
}

struct simd_bellman_ford {
	simd_bellman_ford(unsigned int batchsize)
	: bs{batchsize}, num_segments((bs + 7)/8) { }

	void run(const csr_matrix &tr, const std::vector<unsigned int> &sources) {
		d.resize(tr.n * num_segments);
		d_new.resize(tr.n * num_segments);

		for(unsigned int b = 0; b < sources.size(); b += bs) {
			for(unsigned int j = 0; j <tr.n; j++) 
			{
				for(unsigned int i = 0; i < num_segments; i++)
				{
					d[j * num_segments + i] = _mm256_set1_ps(FLT_MAX);
				}
			}

			for(unsigned int k = 0; k < bs; ++k) {
				auto s = sources[b + k];
				auto index = s * num_segments + k / 8;
				d[index][k%8] = 0;
			}

			bool changes = false;
			bool another_iter;
			unsigned int iters = 0;

			#pragma omp parallel
			{
				do {
					#pragma omp for reduction(||: changes)
					for(unsigned int v = 0; v < tr.n; ++v) {

						//[todo]
						for(unsigned int k = 0; k < num_segments; ++k)
							d_new[v * num_segments + k] = d[v * num_segments + k];

						//[todo]
						for(unsigned int i = tr.ind[v]; i < tr.ind[v + 1]; ++i) {
							auto u = tr.cols[i]; // Kante: v -> u in tr, u -> v im Input.
							auto weight = tr.weights[i];

							for(unsigned int k = 0; k < num_segments; ++k) {
								update_segment(weight, d[u * num_segments + k],
										d_new[v * num_segments + k], changes);
							}
						}
					}

					#pragma omp single
					{
						std::swap(d, d_new);
						another_iter = changes;
						changes = false;
						++iters;
					}
				} while(another_iter);
			}
			std::cerr << "bf took " << iters << " iterations" << std::endl;
		}
	}

private:
	inline void update_segment(float weight,const  __m256& d, __m256& d_new, bool& changes)
	{
		__m256 w = _mm256_set1_ps(weight);
		w = _mm256_add_ps(w, d);
		__m256 res = _mm256_min_ps(w, d_new);
		changes |= !eq_vec(res, d_new);
		d_new = res;
	}
	unsigned int bs;
	unsigned int num_segments;
	std::vector<__m256> d;
	std::vector<__m256> d_new;
};



struct batch_bellman_ford {
	batch_bellman_ford(unsigned int batchsize)
	: bs{batchsize} { }

	void run(const csr_matrix &tr, const std::vector<unsigned int> &sources) {
		d.resize(tr.n * bs);
		d_new.resize(tr.n * bs);

		for(unsigned int b = 0; b < sources.size(); b += bs) {
			std::fill(d.begin(), d.end(), FLT_MAX);
			for(unsigned int k = 0; k < bs; ++k) {
				auto s = sources[b + k];
				d[s * bs + k] = 0;
			}

			bool changes = false;
			bool another_iter;
			unsigned int iters = 0;

			#pragma omp parallel
			{
				do {
					#pragma omp for reduction(||: changes)
					for(unsigned int v = 0; v < tr.n; ++v) {
						for(unsigned int k = 0; k < bs; ++k)
							d_new[v * bs + k] = d[v * bs + k];

						for(unsigned int i = tr.ind[v]; i < tr.ind[v + 1]; ++i) {
							auto u = tr.cols[i]; // Kante: v -> u in tr, u -> v im Input.
							auto weight = tr.weights[i];

							for(unsigned int k = 0; k < bs; ++k) {
								if(d_new[v * bs + k] > d[u * bs + k] + weight) {
									d_new[v * bs + k] = d[u * bs + k] + weight;
									changes = true;
								}
							}
						}
					}

					#pragma omp single
					{
						std::swap(d, d_new);
						another_iter = changes;
						changes = false;
						++iters;
					}
				} while(another_iter);
			}

			std::cerr << "bf took " << iters << " iterations" << std::endl;
		}
	}

private:
	unsigned int bs;
	std::vector<float> d;
	std::vector<float> d_new;
};

static const char *usage_text =
	"Usage: hashing [OPTIONS]\n"
	"Possible OPTIONS are:\n"
	"    --batch BATCH\n"
	"    --algo ALGORITHM\n"
	"        Select an algorithm {simd-bf, batch-bf}.\n"
	"    --instance INSTANCE\n";

int main(int argc, char **argv) {

	std::string algo;
	std::string instance;
	unsigned int batchsize;

	auto error = [] (const char *text) {
		std::cerr << usage_text << "Usage error: " << text << std::endl;
		exit(2);
	};

	const char *arg;
	char **p = argv + 1;
	auto handle_unary_option = [&] (const char *name) -> bool {
		assert(*p);
		if(std::strcmp(*p, name))
			return false;
		++p;
		if(!(*p))
			error("expected argument for unary option");
		arg = *p;
		++p;
		return true;
	};

	while(*p && !std::strncmp(*p, "--", 2)) {
		if(handle_unary_option("--algo")) {
			algo = arg;
		}else if(handle_unary_option("--instance")) {
			instance = arg;
		}else if(handle_unary_option("--batch")) {
			batchsize = std::atoi(arg);
		}else{
			error("unknown command line option");
		}
	}

	if(*p)
		error("unexpected arguments");

	std::mt19937 prng{42};
	std::uniform_real_distribution<float> weight_distrib{0.0f, 1.0f};

	// Load the graph.
	std::cout << "instance: " << instance << std::endl;
	std::cout << "threads: " << omp_get_max_threads() << std::endl;
	std::cout << "batchsize: " << batchsize << std::endl;
	std::cout << "algorithm: " << algo << std::endl;

	std::ifstream ins(instance);
	std::vector<std::tuple<unsigned int, unsigned int, float>> cv;

	auto io_start = std::chrono::high_resolution_clock::now();
	read_graph_unweighted(ins, [&] (unsigned int u, unsigned int v) {
		// Generate a random edge weight in [a, b).
		cv.push_back({u, v, weight_distrib(prng)});
	});

	auto mat = coordinates_to_csr(std::move(cv));
	auto t_io = std::chrono::high_resolution_clock::now() - io_start;

	std::cout << "time_io: "
			<< std::chrono::duration_cast<std::chrono::milliseconds>(t_io).count() << std::endl;
	std::cout << "n_nodes: " << mat.n << std::endl;
	std::cout << "n_edges: " << mat.nnz << std::endl;

	// Compute the transpose (for Bellman-Ford).
	auto tr = transpose(mat);

	// Generate random sources.
	std::uniform_int_distribution<unsigned int> s_distrib{0, mat.n - 1};
	std::vector<unsigned int> sources;
	for(unsigned int i = 0; i < batchsize; ++i)
		sources.push_back(s_distrib(prng));

	// Run the algorithm.
	auto algo_start = std::chrono::high_resolution_clock::now();
	if(algo == "batch-bf") {
		batch_bellman_ford algo{batchsize};
		algo.run(tr, sources);
	}else if(algo ==  "simd-bf") {
		simd_bellman_ford algo{batchsize};
		algo.run(tr, sources);
	}else{
		throw std::runtime_error("Unexpected algorithm");
	}
	auto t_algo = std::chrono::high_resolution_clock::now() - algo_start;

	std::cout << "time_mssp: "
			<< std::chrono::duration_cast<std::chrono::milliseconds>(t_algo).count() << std::endl;
	std::cout << "time_mssp_per_source: "
			<< std::chrono::duration_cast<std::chrono::microseconds>(t_algo).count()
			/ batchsize / 1000.0f << std::endl;
}
