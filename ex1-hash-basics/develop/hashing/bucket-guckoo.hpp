#pragma once

#include <climits>
#include <utility>
#include <random>

extern const int M;
const int OPT_GUCKOO_THRESHOLD = 200;


constexpr int wordlen(int a){
	assert(a != 0);
        return __builtin_clz(1) - __builtin_clz(a);
}

// This hash table uses bucket guckoo
// d is number of hash functions to use.
// B is size of each bucket.
template <int d, int B>
struct guckoo_table {
	static constexpr const char *name = "Bucket-Guckoo";

	guckoo_table(unsigned long threshold = OPT_GUCKOO_THRESHOLD)
		: cells{new cell[M]{}}, other{new cell[M]{}},
			prng{}, threshold(threshold)
	{
		int b = B;
		while(b > 1)
		{
			assert(! (b&1));
			b>>=1;
		}

		std::uniform_int_distribution<int> draw_a(0, INT_MAX - 1);
		std::uniform_int_distribution<int> draw_b(0, (1<< (wordsize - len_num_of_blocks)) - 1);
		for(int i = 0; i < d; i++)
		{
			hash_fnc[i] = std::make_pair(draw_a(prng), draw_b(prng));
		}
	}
	~guckoo_table()
	{
		free(cells);
		free(other);
	}

	struct cell
	{
		cell() = default;
		cell(int key, int value, bool valid = false):
			key{key}, value{value}, valid{valid}{}
		int key;
		int value;
		bool valid = false;
	};

	bool try_add_to_bucket(int k, unsigned int bucket, int v)
	{
		for(int i = 0; i < B; i++)
		{
			int idx = bucket * B + hash_to_index(k + i);
			assert(idx < M);
			cell &c = cells[idx];

			if(!c.valid) {
				c = cell(k, v, true);
				return true;
			}

			if(c.key == k) {
				c.value = v;
				return true;
			}
		}
		return false;
	}
	void put(int k, int v) {
		std::uniform_int_distribution<int> draw_func(0, d-1);
		std::uniform_int_distribution<int> draw_index(0, B-1);

		while(1)
		{
			for(int trial = 0; trial < threshold; trial++)
			{
				for(int i = 0; i < d; i++)
				{
					if( try_add_to_bucket(k, get_bucket_index(k, hash_fnc[i]), v))
					{
						return;
					}
				}
				
				int f = draw_func(prng);
				int b_ind = draw_index(prng);
				int ind = get_bucket_index(k, hash_fnc[f]) * B + b_ind;
				assert(ind < M);
				cell temp = cells[ind];
				cells[ind] = cell(k, v, true);
				k = temp.key;
				v = temp.value;
			}
			regenerate_table();
		}
	}

	std::optional<int> get_in_bucket(int k, int bucket) {
		for(int i = 0; i < B; i++)
		{

			auto idx = bucket * B + hash_to_index(k + i);
			assert(idx < M);
			auto &c = cells[idx];

			if(!c.valid)
				return std::nullopt;

			if(c.key == k)
				return c.value;

			//++i;
		}
		return std::nullopt;
	}
	std::optional<int> get(int k) {
		for(int i = 0; i < d; i++)
		{
			auto ret = get_in_bucket(k, get_bucket_index(k, hash_fnc[i]));
			if(ret != std::nullopt)
			{
				return ret;
			}
		}
		return std::nullopt;
	}
	
	void regenerate_table()
	{
		cell *tmp = other;
		other = cells;
		cells = tmp;
		std::fill(cells, cells + M, cell{});
		for(int i = 0; i < M; i++)
		{
			if(other[i].valid)
			{
				put(other[i].key, other[i].value);
			}
		}
	}
private:
	int hash_to_index(long long int h) {
		return (int)(h & (B - 1));
	}

	int hash_to_block(long long int h) {
		return (int)(h & (num_of_blocks - 1));
	}
	
	// get a random bucket using the universal family of hash functions shown
	// in the lecture: (ax + b) >> (w-l) where a is odd and b < 1<<(w-l) .
	// Make a odd by multiplying by 2 and adding 1.
	int get_bucket_index(int k, std::pair<int, int> func) {
		assert(func.second < 1 << (wordsize - len_num_of_blocks));
		long long int val = (func.first * 2ll + 1) * k + func.second;
		return hash_to_block((val >> (wordsize - len_num_of_blocks)));
	}

	cell *cells = nullptr;
	cell *other = nullptr;

	std::mt19937 prng;
	std::pair<int, int> hash_fnc[d];

	int threshold;

	constexpr static int wordsize = sizeof(int) * 8 - 1; // we use signed ints...
	constexpr static int num_of_blocks = M / B;
	constexpr static int len_num_of_blocks = wordlen(num_of_blocks);

};
