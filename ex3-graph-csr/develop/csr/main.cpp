#include <omp.h>
#include <algorithm>
#include <mutex>
#include <queue>
#include <cassert>
#include <cstring>
#include <iostream>
#include <fstream>
#include <random>
#include <sstream>
#include <string>
#include <tuple>
#include <vector>
#include <climits>

#include <chrono>

constexpr unsigned int INF = UINT_MAX;

template<typename F>
void read_graph_unweighted(std::istream &ins, F fn) {
	std::string line;
	bool seen_header = false;
	while (std::getline(ins, line)) {
		if(line.empty())
			continue;
		if(line.front() == '%')
			continue;

		std::istringstream ls(line);
		unsigned int u, v;
		if (!(ls >> u >> v))
			throw std::runtime_error("Parse error while reading input graph");

		if(!seen_header) {
			seen_header = true;
			continue;
		}

		fn(u, v);
	}
}

struct csr_matrix {
	unsigned int n;
	unsigned int m;
	std::vector<unsigned int> ind;
	std::vector<unsigned int> cols;
	std::vector<float> weights;
};

csr_matrix coordinates_to_csr(unsigned int n,
		std::vector<std::tuple<unsigned int, unsigned int, float>> cv) {
	unsigned int m = cv.size();

	csr_matrix mat;
	mat.n = n;
	mat.m = m;
	mat.ind.resize(n + 2);
	mat.cols.resize(m);
	mat.weights.resize(m);

	// Count the number of neighbors of each node.
	for(auto ct : cv) {
		auto u = std::get<0>(ct);
		++mat.ind[u];
	}

	// Form the prefix sum.
	for(unsigned int i = 1; i <= n; ++i)
		mat.ind[i] += mat.ind[i - 1];
	assert(mat.ind[n] == m);

	// Insert the entries of the matrix in reverse order.
	for(auto it = cv.rbegin(); it != cv.rend(); ++it) {
		auto u = std::get<0>(*it);
		auto v = std::get<1>(*it);
		auto weight = std::get<2>(*it);
		mat.cols[mat.ind[u] - 1] = v;
		mat.weights[mat.ind[u] - 1] = weight;
		--mat.ind[u];
	}
	mat.ind[n+1] = m;
	return mat;
}


csr_matrix transpose(const csr_matrix& mat)
{
	unsigned int n = mat.n;
	unsigned int m = mat.m;
	csr_matrix out;
	out.n = n;
	out.m = m;
	out.ind.assign(mat.ind.size(), 0);
	out.cols.assign(mat.cols.size(), 0);
	out.weights.assign(mat.weights.size(), 0);
	assert(mat.ind.size() == mat.n + 2);

	for(unsigned int i = 0; i < m; i++)
	{
		out.ind[mat.cols[i]]++;
	}
	for(unsigned int i = 1; i <= n+1; i++)
	{
		out.ind[i]+=out.ind[i-1];
	}
	assert(out.ind[n + 1] == m);
	for(unsigned int i = n; i >= 1; i--)
	{
		for(unsigned int j = mat.ind[i]; j < mat.ind[i+1]; j++)
		{
			int v = mat.cols[j];
			out.cols[--out.ind[v]] = i;
		}
	}
	assert(out.ind[1] == 0);
	return out;
}

std::ostream& operator<<(std::ostream& out, const csr_matrix& mat)
{
	out << "++++++++++++++++++++++++++++++\n";
	out << mat.n << "\t" << mat.m << std::endl;
	for(unsigned int i = 0; i <= mat.n; i++)
	{
		out << i << ":\t";
		for(unsigned int j = mat.ind[i]; j < mat.ind[i+1]; j++)
		{
			out << mat.cols[j] << "\t";
		}
		out << std::endl;
	}
	out << "\n++++++++++++++++++++++++++++++" << std::endl;
	return out;
}

struct cmpr
{
	bool operator()(const std::pair<unsigned int, unsigned int> a,
			const std::pair<unsigned int, unsigned int> b)
	{
			return a.second > b.second; 
	}
};
void bellman_ford(unsigned int src, unsigned int* const dist, const csr_matrix& mat)
{
	std::fill(dist, dist + mat.n + 1, INF);
	dist[src] = 0;
	std::vector<std::mutex> mutexes(mat.n + 1);
	unsigned int t = 0;
	bool relaxed = true;
	while(relaxed)
	{
		t++; 
		relaxed = false;
		
		#pragma omp parallel for
		for(unsigned int u = 0; u <= mat.n; u++)
		{
			#pragma omp parallel for
			for(unsigned int k  = mat.ind[u]; k < mat.ind[u+1]; k++)
			{
				unsigned int v = mat.cols[k];
				unsigned int w = mat.weights[k];
				if(dist[u] + w < dist[v])
				{
					relaxed = true;
					std::lock_guard lock{mutexes[v]};
					// no need to lock u since it only gets smaller
					if(dist[u] + w < dist[v])
					{
						dist[v] = dist[u] + w;
					}
				}
			}

		}

	}
	assert(t <= mat.n);
}

void dijkstra(unsigned int src, unsigned int* const  dist, const csr_matrix& mat)
{
	std::priority_queue<std::pair<unsigned int, unsigned int>,
		std::vector<std::pair<unsigned int, unsigned int> >, cmpr> q;
	unsigned int n = mat.n;
	std::fill(dist, dist + n + 1, INF);
	dist[src] = 0;
	q.push(std::make_pair(src, 0));
	while(!q.empty())
	{
		auto p = q.top();
		q.pop();
		auto u = p.first;
		auto d = p.second;
		if(d > dist[u])
		{
			continue;
		}
		for(unsigned int i =  mat.ind[u]; i < mat.ind[u+1]; i++)
		{
			unsigned int v = mat.cols[i];
			unsigned int d = mat.weights[i];
			if(d + dist[u] < dist[v])
			{
				dist[v] = d + dist[u];
				q.push(std::make_pair(v, dist[v]));
			}
		}
	}
}

// Helper function to perform a microbenchmark.
// You should not need to touch this.
void microbenchmark(
	std::string graph, std::string algo_name, 
	const csr_matrix &mat, int num_invocs, void (*algo)(unsigned int, unsigned int* const, const csr_matrix&)
) {
	// Algo table{max_fill, num_tables};

	std::mt19937 prng{42};
	std::uniform_int_distribution<int> distrib(1, mat.n);

	std::cerr << "Running microbenchmark..." << std::endl;

	unsigned int** dist = new unsigned int*[num_invocs];
	for(int i = 0; i < num_invocs; ++i) {
		dist[i] = new unsigned int[mat.n];
	}
	auto start = std::chrono::high_resolution_clock::now();
	#pragma omp parallel for
	for(int i = 0; i < num_invocs; ++i) {
		unsigned int src = distrib(prng);
		algo(src, dist[i], mat);
	}
	auto t = std::chrono::high_resolution_clock::now() - start;

	std::cout << "algo: " << algo_name << std::endl;
	std::cout << "graph: " << graph << std::endl;
	std::cout << "num_invocs: " << num_invocs << std::endl;
	std::cout << "num_threads: " << omp_get_max_threads() << std::endl;
	std::cout << "time: "
			<< std::chrono::duration_cast<std::chrono::milliseconds>(t).count() << std::endl;
}

static const char *usage_text =
	"Usage: hashing [OPTIONS]\n"
	"Possible OPTIONS are:\n"
	"    --algo ALGORITHM\n"
	"        Select an algorithm {transpose, dijkstra, bellman-ford}.\n"
	"    --graph FACTOR\n"
	"        Select the input graph {cit-patent.edges, roadNet-TX.mtx}.\n"
	"    --num-invocations NUMBER\n"
	"        Set the number of invocations.\n";

int main(int argc, char **argv) {
	std::string algorithm;
	std::string graph;
	int num_invocs = 1;

	auto error = [] (const char *text) {
		std::cerr << usage_text << "Usage error: " << text << std::endl;
		exit(2);
	};

	// Argument for unary options.
	const char *arg;

	// Parse all options here.

	char **p = argv + 1;

	auto handle_unary_option = [&] (const char *name) -> bool {
		assert(*p);
		if(std::strcmp(*p, name))
			return false;
		++p;
		if(!(*p))
			error("expected argument for unary option");
		arg = *p;
		++p;
		return true;
	};

	while(*p && !std::strncmp(*p, "--", 2)) {
		if(handle_unary_option("--algo")) {
			algorithm = arg;
		}else if(handle_unary_option("--graph")) {
			graph = arg;
		}else if(handle_unary_option("--num-invocations")) {
			num_invocs = std::atoi(arg);
		}else{
			error("unknown command line option");
		}
	}

	if(*p)
		error("unexpected arguments");


	std::ifstream ins(graph);
	std::vector<std::tuple<unsigned int, unsigned int, float>> cv;
	std::mt19937 prng{42};
	std::uniform_real_distribution<float> distrib{0.0f, 1.0f};
	read_graph_unweighted(ins, [&] (unsigned int u, unsigned int v) {
		// Generate a random edge weight in [a, b).
		cv.push_back({u, v, distrib(prng)});
	});

	// Determine n as the maximal node ID.
	unsigned int n = 0;
	for(auto ct : cv) {
		auto u = std::get<0>(ct);
		auto v = std::get<1>(ct);
		if(u > n)
			n = u;
		if(v > n)
			n = v;
	}

	auto mat = coordinates_to_csr(n, std::move(cv));

	//cv.clear();
	//unsigned int m;
	//std::cin >> n >> m;
	//for(unsigned int i = 0; i < m; i++)
	//{
	//	unsigned int x, y, z;
	//	std::cin >> x >> y >> z;
	//	cv.push_back(std::make_tuple(x, y, z));
	//}


	// EVALUATE TRANSPOSE
	if(algorithm == "transpose") {
		auto t1 = std::chrono::high_resolution_clock::now();
		auto trans = transpose(mat);
		auto t2 = std::chrono::high_resolution_clock::now();
		auto duration = std::chrono::duration_cast<std::chrono::milliseconds>( t2 - t1 ).count();
		// std::cout << mat << std::endl;
		// std::cout << trans << std::endl;
		std::cout << "Time needed for transpose:" << duration << "ms" << std::endl;
	} else if(algorithm == "dijkstra") {
		microbenchmark(graph, "dijkstra", mat, num_invocs, dijkstra);
	} else if(algorithm == "bellman-ford") {
		microbenchmark(graph, "bellman-ford", mat, num_invocs, bellman_ford);
	}

	// unsigned int* dist = new unsigned int[n + 1];
	// unsigned int* dist2 = new unsigned int[n + 1];
	// dijkstra(1, dist, mat);

	// bellman_ford(1, dist2, mat);
	// for(unsigned int i =1; i <= n; i++)
	// {
	// 	assert(dist[i] == dist2[i]);
	// }
}
